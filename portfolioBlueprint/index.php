<!DOCTYPE html>
<html>
<head>
	<title> Full Stack Web | Pierre Victor</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="style.css">
	<link href="https://fonts.googleapis.com/css?family=Hammersmith+One|Hind|Noto+Sans|Raleway|Roboto" rel="stylesheet"> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>
<body>

	<div class="menu-hamburger">
		<img class="hamb-open" src="images/menuburgericons.png">
		<img class="hamb-close" src="images/menuburgericons2.png">
	</div>

	<div class="sticky-menu">
		<div class="nav-top" id="top-section"><h2>Top</h2></div>
		<div class="nav-past" id="past-section"><h2>Past Projects</h2></div>
		<div class="nav-code" id="skill-section"><h2>Coding Languages</h2></div>
		<div class="nav-contact" id="contact-section"><h2>Contact Me</h2></div>
	</div>

	<section id="hero-section">

		<div class="inner-content">

			<div class="hero-text">

				<h1>Header Text Here!</h1>

				<p>Quick Description of Me Here!</p>
				
			</div>

			<div class="hero-nav-buttons">

				<button class="nav-past" id="past-work-button">Past Projects</button>
				<button class="nav-code" id="coding-languages-button">Coding Languages</button>
				<button class="nav-contact" id="contact-me-button">Contact Me</button>
				
			</div>
			
		</div>
		
	</section>

	<section id="past-work">

		<div id="pw-modal" class="modal">
			<!--Loads up post containing info-->
			<p>Ajax Load Project Page Here</p>
		</div>

		<div class="projects-display">

			<!--Pretending there are six projects-->
			<!--TODO: change this to fit Grav Twig Templating standards later-->
			<?php for($i=0; $i<6; $i++){ ?>

			<div class="project-block">

				<img class="proj-thumbnail" src="images/project-thumb.jpeg">

				<div class="proj-overlay">

					<div class="p-overlay-text">
						<span class="proj-overlay-title">Title <?php echo $i+1; ?> Here</span>
						<a href="#pw-modal" rel="modal:open"><button class="proj-overlay-viewbutton" type="button">View Project</button></a>
					</div>

				</div>
				
			</div>

			<?php } ?>
			
		</div>
		
	</section>

	<section id="coding-languages">

		<div class="cd-sections" id="backend-section">

			<div class="cdsection-info">

				<div class="cdsection-info-title"><h2>Backend Languages</h2></div>

				<div class="cdsection-info-content">
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dictum turpis diam. Maecenas condimentum leo dolor. Mauris eu mollis dolor. Suspendisse vitae massa purus. Aliquam at erat eu eros blandit pharetra vel eget lorem. Aliquam quis leo fringilla lacus sagittis mattis. Maecenas volutpat libero id nisi viverra, ac varius augue vehicula. 
					</p>
				</div>

				<div class="read-more-row"><button class="read-more-button" data-tab-number="1" type="button">Read More</button></div>
				
			</div>

			<div class="cdsection-thumbnail" style="background-image: url('images/php.jpeg'); background-size: cover;">
				
			</div>
			
		</div>

		<div class="cd-sections" id="frontend-section">

			<div class="cdsection-info">

				<div class="cdsection-info-title"><h2>Frontend Languages</h2></div>

				<div class="cdsection-info-content">
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dictum turpis diam. Maecenas condimentum leo dolor. Mauris eu mollis dolor. Suspendisse vitae massa purus. Aliquam at erat eu eros blandit pharetra vel eget lorem. Aliquam quis leo fringilla lacus sagittis mattis. Maecenas volutpat libero id nisi viverra, ac varius augue vehicula. 
					</p>
				</div>

				<div class="read-more-row"><button class="read-more-button" data-tab-number="2" type="button">Read More</button></div>
				
			</div>

			<div class="cdsection-thumbnail" style="background-image: url('images/frontend.jpeg'); background-size: cover;">
				
			</div>
			
		</div>

		<div class="cd-sections" id="misc-section">

			<div class="cdsection-info">

				<div class="cdsection-info-title"><h2>Miscellaneous Skills</h2></div>

				<div class="cdsection-info-content">
					<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce dictum turpis diam. Maecenas condimentum leo dolor. Mauris eu mollis dolor. Suspendisse vitae massa purus. Aliquam at erat eu eros blandit pharetra vel eget lorem. Aliquam quis leo fringilla lacus sagittis mattis. Maecenas volutpat libero id nisi viverra, ac varius augue vehicula. 
					</p>
				</div>

				<div class="read-more-row"><button class="read-more-button" data-tab-number="3" type="button">Read More</button></div>
				
			</div>

			<div class="cdsection-thumbnail" style="background-image: url('images/misc-pic.jpeg'); background-size: cover;">
				
			</div>
			
		</div>
		
	</section>

	<section id="contact-me">

		<div id="my-contact-form">

			<form method="POST" action="">

				<div class="contact-title">
					<h3>Contact Me</h3>
				</div>

				<fieldset class="name-field">
					<input type="text" name="contact-first-name" placeholder="First Name" required>
					<input type="text" name="contact-last-name" placeholder="Last Name" required>
				</fieldset>

				<fieldset>
					<input type="email" name="contact-email" placeholder="Email Address" required>
				</fieldset>

				<fieldset>

					<select>
						<option value="new-web">New Website</option>
						<option value="web-maint">Website Maintenance</option>
						<option value="web-upgr">Website Upgrade/Migration</option>
						<option value="api-dev">API Development</option>
					</select>

				</fieldset>

				<div class="captcha-area">
					Enter Captcha Here
				</div>

				<div class="submit-row">
					<input type="submit" name="" value="SUBMIT">
				</div>
				
			</form>
			
		</div>
		
	</section>
	
	<script type="text/javascript" src="main.js"></script>
</body>
</html>