$(document).ready(function(){
	//Page DOM element caching
	var menu_hamburger = $('.menu-hamburger');
	var hamb_open = $('.hamb-open');
	var hamb_close = $('.hamb-close');
	var sticky_menu = $('.sticky-menu');
	var read_more_button = $('.read-more-button');

	var nav_top = $('.nav-top');
	var nav_past = $('.nav-past');
	var nav_code = $('.nav-code');
	var nav_contact = $('.nav-contact');

	var hero_section = $('#hero-section');
	var past_work = $('#past-work');
	var coding_languages = $('#coding-languages');
	var contact_me = $('#contact-me');

	var backend_section = $('#backend-section');
	var frontend_section = $('#frontend-section');
	var misc_section = $('#misc-section');

	var backend_section_button = $('#backend-section .cdsection-info .read-more-row button');
	var frontend_section_button = $('#frontend-section .cdsection-info .read-more-row button');
	var misc_section_button = $('#misc-section .cdsection-info .read-more-row button');

	var pw_modal = $('#pw-modal');

	//Page variables
	var display_sticky_menu = false;
	var first_time_past_work = true;
	var first_time_coding_languages = true;
	var first_time_contact = true;

	var mobile_view = null;

	var currently_hero_section = true;
	var currently_past_work = false;
	var currently_coding_languages = false;
	var currently_contact = false;

	var skill_tab_expanded_1 = false;
	var skill_tab_expanded_2 = false;
	var skill_tab_expanded_3 = false;

	//hide the sticky menu on load 
	sticky_menu.hide();

	//hide section content
	past_work.hide();
	past_work.children().hide();
	coding_languages.hide();
	coding_languages.children().hide();
	contact_me.hide();
	contact_me.children().hide();

	//toggle sticky menu
	menu_hamburger.click(function(){
		display_sticky_menu = !display_sticky_menu;

		if(display_sticky_menu == false){
			sticky_menu.slideUp(300);
			hamb_close.hide();
			hamb_open.show();
		}else{
			sticky_menu.slideDown(300);
			hamb_open.hide();
			hamb_close.show();
		}

	});

	//responsive menu depending on window width


	//navigate to different page sections on button click
	nav_top.click(function(){

		if(currently_hero_section == true){

		}else{
			
			//close whichever section the end user is currently located
			if(currently_past_work == true){
				past_work.children().fadeOut(300);
				past_work.animate({ height: '0' });
			}else if(currently_coding_languages == true){
				coding_languages.children().fadeOut(300);
				coding_languages.animate({ height: '0' });
			}else if(currently_contact == true){
				contact_me.children().fadeOut(300);
				contact_me.animate({ height: '0' });
			}

			//set section variables
			currently_past_work = false;
			currently_hero_section = true;
			currently_coding_languages = false;
			currently_contact = false;
			
			//show current section
			hero_section.show();
			hero_section.children().fadeIn(700);
			hero_section.animate({ height: '100vh' });

		}

	});

	nav_past.click(function(){

		if(currently_past_work == true){

		}else{

			//close whichever section the end user is currently located
			if(currently_hero_section == true){
				hero_section.children().fadeOut(300);
				hero_section.animate({ height: '0' });
			}else if(currently_coding_languages == true){
				coding_languages.children().fadeOut(300);
				coding_languages.animate({ height: '0' });
			}else if(currently_contact == true){
				contact_me.children().fadeOut(300);
				contact_me.animate({ height: '0' });
			}

			//set section variables
			currently_past_work = true;
			currently_hero_section = false;
			currently_coding_languages = false;
			currently_contact = false;
			
			//show current section
			past_work.show();
			past_work.children().fadeIn(700);
			pw_modal.hide();

			if($(window).width() > 720){
				past_work.animate({ height: '100vh' });
			}else{
				past_work.animate({ height: '200vh' });
			}

		}
		
	});

	nav_code.click(function(){

		if(currently_coding_languages == true){

		}else{

			//close whichever section the end user is currently located
			if(currently_hero_section == true){
				hero_section.children().fadeOut(300);
				hero_section.animate({ height: '0' });
			}else if(currently_past_work == true){
				past_work.children().fadeOut(300);
				past_work.animate({ height: '0' });
			}else if(currently_contact == true){
				contact_me.children().fadeOut(300);
				contact_me.animate({ height: '0' });
			}

			//set section variables
			currently_past_work = false;
			currently_hero_section = false;
			currently_coding_languages = true;
			currently_contact = false;
			
			//show current section
			coding_languages.show();
			coding_languages.children().fadeIn(700);
			
			if($(window).width() > 720){
				coding_languages.animate({ height: '100vh' });
			}else{
				coding_languages.animate({ height: '250vh' });
			}

		}

	});

	nav_contact.click(function(){

		if(currently_contact == true){

		}else{

			//close whichever section the end user is currently located
			if(currently_hero_section == true){
				hero_section.children().fadeOut(300);
				hero_section.animate({ height: '0' });
			}else if(currently_coding_languages == true){
				coding_languages.children().fadeOut(300);
				coding_languages.animate({ height: '0' });
			}else if(currently_past_work == true){
				past_work.children().fadeOut(300);
				past_work.animate({ height: '0' });
			}

			//set section variables
			currently_past_work = false;
			currently_hero_section = false;
			currently_coding_languages = false;
			currently_contact = true;
			
			//show current section
			contact_me.show();
			contact_me.children().fadeIn(700);
			contact_me.animate({ height: '100vh' });

		}

	});

	read_more_button.click(function(e){

		if($(this).data('tab-number') == 1){

			skill_tab_expanded_1 = !skill_tab_expanded_1;

			if(skill_tab_expanded_1 == true){

				backend_section.css({ 'grid-template-rows': '100% 0' });
				backend_section_button.text('Collapse');

			}else{
				backend_section.css({ 'grid-template-rows': '50% 50%' });
				backend_section_button.text('Read More');
			}
			
		}else if($(this).data('tab-number') == 2){

			skill_tab_expanded_2 = !skill_tab_expanded_2;

			if(skill_tab_expanded_2 == true){

				frontend_section.css({ 'grid-template-rows': '100% 0' });
				frontend_section_button.text('Collapse');

			}else{
				frontend_section.css({ 'grid-template-rows': '50% 50%' });
				frontend_section_button.text('Read More');
			}

		}else if($(this).data('tab-number') == 3){

			skill_tab_expanded_3 = !skill_tab_expanded_3;

			if(skill_tab_expanded_3 == true){

				misc_section.css({ 'grid-template-rows': '100% 0' });
				misc_section_button.text('Collapse');

			}else{
				misc_section.css({ 'grid-template-rows': '50% 50%' });
				misc_section_button.text('Read More');
			}

		}

	});

	//window width resize actions
	$(window).resize(function(e){

		if($(window).width() > 720){

		}else{

		}

	});
	
});