<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@ad_dashboard');

Route::resources([
	'posts' => 'PostController',
	'post_categories' => 'CategoryController',
	'images' => 'ImageController',
	'videos' => 'VideoController',
	'series' => 'SeriesController',
	'packages' => 'PackageController'
]);

//Clients
Route::get('/clients', 'ClientController@index');
Route::get('/client_profile', 'ClientController@create');
