@extends('layouts.backend.default')

@section('content')

<div class="all_images_section">
	
	<div class="image_dropzone">
		
	</div>

	@include('partials.filters.image_filter')

	<div class="all_images_display">

		@for($i=0; $i<3; $i++)
		<div class="display_image_row">
			
			@for($j=0; $j<3; $j++)
			<div class="display_image_result">

				<div class="dir_image">
					<img class="" src=""/>
				</div>

				<div class="dir_info">
					<h2>Image Title</h2>
					<p>Author</p>
					<p><strong>Date Updated:</strong> 01-01-2018</p>

					<div class="dir_buttons">
						<button>EDIT</button>
						<button>DELETE</button>
					</div>
				</div>

			</div>
			@endfor

		</div>
		@endfor
		
	</div>

	@include('partials.pagination.img_pagination')

</div>

@endsection