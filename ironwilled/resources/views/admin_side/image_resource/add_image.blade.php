@extends('layouts.backend.default')

@section('content')

	<div class="add_image_section">
	
		<div class="add_image_form">
			<form class="ai_form" method="POST" action="/images">
				{{ csrf_field() }}
				<h1><strong>New Image</strong></h1>

				<fieldset class="ai_form_title">

					<h3>Title</h3>
					<input type="text" name="aif_title"><br>
					<span><strong>Machine Name: </strong>image_slug</span>

				</fieldset>

				<fieldset class="ai_image_area">

					<img src="https://images.pexels.com/photos/703012/pexels-photo-703012.jpeg?w=1260&h=750&dpr=2&auto=compress&cs=tinysrgb" style="width: 530px; height: 330px;">
					
				</fieldset>

				<fieldset class="ai_form_body">

					<h3>Image Description</h3>
					<textarea></textarea>
					
				</fieldset>

				<div class="ai_form_finish_buttons">
					<button class="aif_submit" type="submit">Submit</button>
					<button class="aif_delete" type="button">Delete</button>
				</div>

			</form>
		</div>

	</div>

@endsection