@extends('layouts.backend.default')

@section('content')

	<div class="add_package_section">
	
		<div class="add_package_form">
			<form class="ap_form" method="post" action="{{ URL::to('packages') }}">
				{{ csrf_field() }}
				<h1><strong>New Package</strong></h1>

				<fieldset class="ap_form_title">

					<h3>Title</h3>
					<input type="text" name="title"><br>
					<span><strong>Machine Name: </strong>package_slug</span>

				</fieldset>

				<fieldset class="ap_form_misc">

					<div class="package_form_status">
						
						<h3>Status</h3>

						<p><strong>Current package Status:</strong> <span class="package_status status_publish">Publish</span></p>

						<select name="status">
							<option value="publish">Publish</option>
							<option value="draft">Draft</option>
						</select>

					</div>

					<div class="package_cover_photo">

						<h3>Cover Photo</h3>

						<div class="cover_photo_preview">

						</div>

						<input type="file" name="cover_photo_submit">

					</div>

					<div class="package_thumbnail_photo">

						<h3>Thumbnail Photo</h3>

						<div class="thumbnail_photo_preview">

						</div>

						<input type="file" name="thumbnail_photo_submit">
						
					</div>
					
				</fieldset>

				<fieldset class="ap_form_body">

					<h3>Main Content</h3>

					<div class="ap_form_body_buttons">
						<button type="button" class="apf_add_text">Add Text</button>
						<button type="button" class="apf_add_image">Add Image</button>
						<button type="button" class="apf_add_video">Add Video</button>
					</div>

					<!--<textarea></textarea>-->
					<input type="hidden" name="description" value="THIS IS DESCRIPTION">
					
				</fieldset>

				<div class="ap_form_finish_buttons">
					<button class="apf_submit" type="submit">Submit</button>
					<button class="apf_delete" type="button">Delete</button>
				</div>

			</form>
		</div>

	</div>

@endsection