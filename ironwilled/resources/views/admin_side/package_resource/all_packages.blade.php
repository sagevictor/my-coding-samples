@extends('layouts.backend.default')

@include('partials.modals.add_edit_paragraph')

@section('content')

	<div class="all_packages_section">

		<h1 class="index_title"><strong>Packages</strong></h1>
		
		@include('partials.filters.package_filter')

		<div class="all_packages_table">
			
			<div class="apat_row apat_headers">
				
				<div class="apat_headers_checkbox"><input type="checkbox" name=""></div>

				<div class="apat_headers_title"><p>Package</p></div>
				<div class="apat_headers_title"><p>Machine Name</p></div>
				<div class="apat_headers_title"><p>Author</p></div>
				<div class="apat_headers_title"><p>Price</p></div>
				<div class="apat_headers_title"><p>Publish Date</p></div>
				<div class="apat_headers_title"><p>Delete</p></div>

			</div>

			@for($i=0; $i<30; $i++)

				<div class="apat_row apat_results">

					<div class="apat_row_checkbox"><input type="checkbox" name=""></div>

					<div class="apat_row_column"><a href="">Sample package</a></div>
					<div class="apat_row_column"><a href="">package_slug</a></div>
					<div class="apat_row_column"><a href="">Example Author</a></div>
					<div class="apat_row_column"><a href="">$30.00</a></div>
					<div class="apat_row_column"><a href="">02/02/2018</a></div>
					<div class="apat_row_column_delete"><button>Delete</button></div>
					
				</div>

			@endfor

		</div>

		@include('partials.pagination.pa_pagination')
		
	</div>

@endsection