@extends('layouts.backend.default')

@section('content')
	<div class="main_section_content">

		@include('partials.admin_header')

		<div class="dash_tables dash_todays_appointments">

			<div class="dt_row dt_title">
				
				<h4>TODAY'S APPOINTMENTS</h4>

			</div>

			<div class="dt_row dt_headers">
				
				<div class="dt_top_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_head_title"><p>Client</p></div>
				<div class="dt_head_title"><p>Phone Number</p></div>
				<div class="dt_head_title"><p>Workout</p></div>
				<div class="dt_head_title"><p>Progress</p></div>

			</div>

			@for($i=0; $i<5; $i++)
			<div class="dt_row dt_results">

				<div class="dt_row_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_row_column">Client 1</div>
				<div class="dt_row_column">555-555-5555</div>
				<div class="dt_row_column">Example Workout 1</div>
				<div class="dt_row_column">Check Progress</div>
				
			</div>
			@endfor

		</div>

		<div class="dash_tables dash_client_list">
			
			<div class="dt_row dt_title">
				
				<h4>CLIENT LIST</h4>

			</div>

			<div class="dt_row dt_headers">
				
				<div class="dt_top_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_head_title"><p>Client</p></div>
				<div class="dt_head_title"><p>Phone Number</p></div>
				<div class="dt_head_title"><p>Workout</p></div>
				<div class="dt_head_title"><p>Progress</p></div>
				<div class="dt_head_title"><p>Contact Client</p></div>

			</div>

			@for($i=0; $i<5; $i++)
			<div class="dt_row dt_results">

				<div class="dt_row_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_row_column"><span>Client 1</span></div>
				<div class="dt_row_column"><span>555-555-5555</span></div>
				<div class="dt_row_column"><span>Example Workout 1</span></div>
				<div class="dt_row_column"><span>Check Progress</span></div>
				<div class="dt_row_column"><span>Contact Client</span></div>
				
			</div>
			@endfor

		</div>

		<div class="dash_tables dash_posts_comments">

			<div class="dt_row dt_title">
				
				<h4>RECENT POSTS AND COMMENTS</h4>

			</div>

			<div class="dt_row dt_headers">
				
				<div class="dt_top_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_head_title"><p>Post Title</p></div>
				<div class="dt_head_title"><p>Date</p></div>
				<div class="dt_head_title"><p>Author</p></div>
				<div class="dt_head_title"><p>Comment</p></div>

			</div>

			@for($i=0; $i<5; $i++)
			<div class="dt_row dt_results">

				<div class="dt_row_checkbox"><input type="checkbox" name=""></div>

				<div class="dt_row_column">Blogpost 1</div>
				<div class="dt_row_column">01/01/2018</div>
				<div class="dt_row_column">Sample Commentor</div>
				<div class="dt_row_column">Here is my comment!</div>
				
			</div>
			@endfor

		</div>

	</div>

@endsection