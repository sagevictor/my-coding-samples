@extends('layouts.backend.default')

@section('content')

	<div class="all_posts_section">

		<h1 class="index_title"><strong>Blog Posts</strong></h1>
		
		@include('partials.filters.post_filter')

		<div class="all_posts_table">
			
			<div class="apt_row apt_headers">
				
				<div class="apt_headers_checkbox"><input type="checkbox" name=""></div>

				<div class="apt_headers_title"><p>Post Title</p></div>
				<div class="apt_headers_title"><p>Author</p></div>
				<div class="apt_headers_title"><p>Status</p></div>
				<div class="apt_headers_title"><p>Series</p></div>
				<div class="apt_headers_title"><p>Publish Date</p></div>
				<div class="apt_headers_title"><p>Delete</p></div>

			</div>

			@for($i=0; $i<30; $i++)

				<div class="apt_row apt_results">

					<div class="apt_row_checkbox"><input type="checkbox" name=""></div>

					<div class="apt_row_column"><a href="">Sample Post</a></div>
					<div class="apt_row_column"><a href="">Example Author</a></div>
					<div class="apt_row_column"><a href="">Publish</a></div>
					<div class="apt_row_column"><a href="">Sample Blog Series</a></div>
					<div class="apt_row_column"><a href="">02/02/2018</a></div>
					<div class="apt_row_column_delete"><button>Delete</button></div>
					
				</div>

			@endfor

		</div>

		@include('partials.pagination.pagination')
		
	</div>

@endsection