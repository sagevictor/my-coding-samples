@extends('layouts.backend.default')

@include('partials.modals.add_edit_paragraph')

@section('content')

	<div class="add_post_section">
	
		<div class="add_post_form">
			<form class="ap_form" method="post" action="{{URL::to('posts')}}">
				{{ csrf_field() }}
				<h1><strong>Edit Blog Post</strong></h1>

				<fieldset class="ap_form_title">

					<h3>Title</h3>
					<input type="text" name="title"><br>
					<span><strong>Shown Url: </strong>http://www.example.com/ironblog/</span>

				</fieldset>

				<fieldset class="ap_form_misc">

					<div class="post_form_status">
						
						<h3>Status</h3>

						<p><strong>Current Post Status:</strong> <span class="post_status status_publish">Publish</span></p>

						<select name="status">
							<option value="publish">Publish</option>
							<option value="draft">Draft</option>
						</select>

					</div>

					<div class="post_cover_photo">

						<h3>Cover Photo</h3>

						<div class="cover_photo_preview">

						</div>

						<button type="button">Upload Image</button>

					</div>

					<div class="post_thumbnail_photo">

						<h3>Thumbnail Photo</h3>

						<div class="thumbnail_photo_preview">

						</div>

						<button type="button">Upload Image</button>
						
					</div>
					
				</fieldset>

				<fieldset class="ap_form_misc2">
					
					<div class="select_type">

						<h3>Select Type</h3>

						<select name="type">
							<option value="type_1">Type 1</option>
							<option value="type_2">Type 2</option>
							<option value="type_3">Type 3</option>
						</select>
						
					</div>

					<div class="select_category">

						<h3>Select Category</h3>

						<select name="category">
							<option value="category_1">Category 1</option>
							<option value="category_2">Category 2</option>
							<option value="category_3">Category 3</option>
						</select>
						
					</div>

					<div class="select_series">

						<h3>Select Series</h3>

						<select name="series">
							<option value="series_1">Series 1</option>
							<option value="series_2">Series 2</option>
							<option value="series_3">Series 3</option>
						</select>
						
					</div>

				</fieldset>

				<fieldset class="ap_form_body">

					<h3>Main Content</h3>

					<div class="ap_form_body_buttons">
						<button type="button" class="apf_add_text">Add Text</button>
						<button type="button" class="apf_add_image">Add Image</button>
						<button type="button" class="apf_add_video">Add Video</button>
					</div>

					<!--<textarea></textarea>-->
					<input type="hidden" name="body" value="THIS IS BODY">
					
				</fieldset>

				<div class="ap_form_finish_buttons">
					<button class="apf_submit" type="submit">Submit</button>
					<button class="apf_delete" type="button">Delete</button>
				</div>

			</form>
		</div>

	</div>

@endsection