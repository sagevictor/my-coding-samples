@extends('layouts.backend.default')

@section('content')

	<div class="all_clients_section">

		<h1 class="index_title"><strong>Clients</strong></h1>
		
		@include('partials.filters.client_filter')

		<div class="all_clients_table">
			
			<div class="act_row act_headers">
				
				<div class="act_headers_checkbox"><input type="checkbox" name=""></div>

				<div class="act_headers_title"><p>Client</p></div>
				<div class="act_headers_title"><p>Machine Name</p></div>
				<div class="act_headers_title"><p>Author</p></div>
				<div class="act_headers_title"><p>Status</p></div>
				<div class="act_headers_title"><p>Publish Date</p></div>
				<div class="act_headers_title"><p>Delete</p></div>

			</div>

			@for($i=0; $i<30; $i++)

				<div class="act_row act_results">

					<div class="act_row_checkbox"><input type="checkbox" name=""></div>

					<div class="act_row_column"><a href="">Sample client</a></div>
					<div class="act_row_column"><a href="">client_slug</a></div>
					<div class="act_row_column"><a href="">Example Author</a></div>
					<div class="act_row_column"><a href="">Publish</a></div>
					<div class="act_row_column"><a href="">02/02/2018</a></div>
					<div class="act_row_column_delete"><button>Delete</button></div>
					
				</div>

			@endfor

		</div>

		@include('partials.pagination.cl_pagination')
		
	</div>

@endsection