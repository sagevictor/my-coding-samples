@extends('layouts.backend.default')

@section('content')

	<div class="main_profile_content">

		<form method="GET" action="">
			{{ csrf_field() }}
			<h1><strong>pvictor Profile</strong></h1>

			<fieldset class="profile_general_info">

				<div class="pgi_pic">
					
				</div>

				<div class="pgi_contact">

					<p class="pgic_name"><strong>Name: </strong><input type="text" name=""></p>

					<p class="pgic_phone"><strong>Phone Number: </strong><input type="text" name=""></p>

					<p class="pgic_email"><strong>Email: </strong><input type="text" name=""></p>

					<!--Members only-->
					<p class="pgic_location_address"><strong>Street Address: </strong><input type="text" name=""></p>
					<!--End of Members only-->

					<p class="pgic_location_city_state_zip">
						<span><strong>City: </strong><input type="text" name=""></span> 
						<span><strong>State: </strong><input type="text" name=""></span>
						<span><strong>Zipcode: </strong><input type="text" name=""></span>
					</p>

					<p><input type="checkbox" name=""> Check only if Billing Address is different from above address</p>

					<!--Billing Address if different than above-->

						<!--Members only-->
						<p class="pgic_billing_address"><strong>Street Address: </strong><input type="text" name=""></p>
						<!--End of Members only-->

						<p class="pgic_billing_city_state_zip">
							<span><strong>City: </strong><input type="text" name=""></span> 
							<span><strong>State: </strong><input type="text" name=""></span>
							<span><strong>Zipcode: </strong><input type="text" name=""></span>
						</p>

					<!--End of Shipping Address if different than above-->
					
				</div>
				
			</fieldset>

			<!--If user is a Coach-->
			<fieldset class="profile_coaching_info">

				<div class="pci_row1">

					<div class="pci_1-1">
						<strong>Coaching Role: </strong>
						<select>
							<option>Admin</option>
							<option>Coach - Admin</option>
							<option>Coach</option>
							<option>Temp Coach</option>
						</select>
					</div>

					<div class="pci_1-2">
						<strong>Coaching Status: </strong>
						<select>
							<option>Active</option>
							<option>On Hold</option>
							<option>Suspended</option>
							<option>Terminated</option>
						</select>
					</div>

					<div class="pci_1-3">
						<strong>Coaching Specialty: </strong><input type="text" name="">
					</div>
					
				</div>
				
				<div class="pci_row2">
					<p>
						<strong>Description: </strong><br>
						<textarea></textarea>
					</p>
				</div>
				
			</fieldset>
			<!--End of If user is a Coach-->

			<!--If user is a Member-->
			<fieldset class="profile_client_info">

				<div class="pcli_row1">

					<p><strong>Coached By: </strong> Sample Coach</p>

					<p>
						<strong>Member Status: </strong>
						<select>
							<option>Active</option>
							<option>On Hold</option>
							<option>Suspended</option>
							<option>Terminated</option>
						</select>
					</p>

					<p>
						<strong>Member Package: </strong>
						<select>
							<option>Basic</option>
							<option>Premium</option>
						</select>
					</p>
					
				</div>

				<div class="pcli_row2">

					<p><strong>Diet Plan: </strong></p>

					<p><strong>Workout Plan: </strong></p>
					
				</div>
				
			</fieldset>
			<!--End of If user is a Member-->

			<div class="profile_submit_buttons">
				
				<button class="psb_submit" type="submit">Submit</button>
				<button class="psb_cancel" type="button">Cancel</button>

			</div>
			
		</form>
		
	</div>

@endsection