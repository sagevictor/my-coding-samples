@extends('layouts.backend.default')

@section('content')

	<div class="all_videos_section">

		<h1 class="index_title"><strong>Videos</strong></h1>
		
		@include('partials.filters.video_filter')

		<div class="all_videos_table">
			
			<div class="av_row avt_headers">
				
				<div class="avt_headers_checkbox"><input type="checkbox" name=""></div>

				<div class="avt_headers_title"><p>Video</p></div>
				<div class="avt_headers_title"><p>Video Info</p></div>
				<div class="avt_headers_title"><p>Delete</p></div>

			</div>

			@for($i=0; $i<30; $i++)

				<div class="avt_row avt_results">

					<div class="avt_row_checkbox"><input type="checkbox" name=""></div>

					<div class="avt_row_column"><a href=""><img src="https://img.youtube.com/vi/n5hS2-pY7Jk/hqdefault.jpg"></a></div>

					<div class="avt_row_column avt_row_info">

						<div class="avt_rc_title"><a href=""><h3>Video Title Here</h3></a></div>
						<div class="avt_rc_author"><a href=""><p>Sample Author</p></a></div>
						<div class="avt_rc_url"><a href=""><p>http://www.example.com</p></a></div>

					</div>

					<div class="avt_row_column">
						<div class="avt_rc_delete"><button>Delete</button></div>
					</div>
					
				</div>

			@endfor

		</div>

		@include('partials.pagination.vi_pagination')
		
	</div>

@endsection