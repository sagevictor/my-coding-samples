@extends('layouts.backend.default')

@section('content')

	<div class="add_video_section">
	
		<div class="add_video_form">
			<form class="av_form" method="post" action="{{ URL::to('videos') }}">
				{{ csrf_field() }}
				<h1><strong>Edit Video</strong></h1>

				<fieldset class="av_form_title">

					<h3>Title</h3>
					<input type="text" name="avf_title"><br>
					<span><strong>Machine Name: </strong>video_slug</span>

				</fieldset>

				<fieldset class="av_embed_area">

					<iframe width="560" height="315" src="https://www.youtube.com/embed/-4u9w0mqqxE" frameborder="0" allow="encrypted-media" allowfullscreen></iframe>
					
				</fieldset>

				<fieldset class="av_form_body">

					<h3>Youtube Video Link</h3>
					<input type="url" name="avf_vid_name">

					<br><br>

					<h3>Video Description</h3>
					<textarea></textarea>
					
				</fieldset>

				<div class="av_form_finish_buttons">
					<button class="avf_submit" type="submit">Submit</button>
					<button class="avf_delete" type="button">Delete</button>
				</div>

			</form>
		</div>

	</div>

@endsection