<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}">
</head>
<body id="app">

	<div class="admin_nav">

		<ul class="nav_list">
			<li><a href="">View Site</a></li>
			<li><a href="/dashboard">Dashboard</a></li>
			<li><a href="/posts/">Blog</a></li>
			<li><a href="/images/create">Media</a></li>
			<li><a href="/clients/">Users</a></li>
			<li><a href="/packages/">Packages</a></li>
		</ul>

		<ul class="admin_log">
			<li><a href="/client_profile">pvictor</a></li>
			<li><a href="/logout"><strong>Log Out</strong></a></li>
		</ul>
	</div>

	<section class="main_section">
		
		@yield('content')

	</section>

	<div class="admin_bottom">
		<p class="bottom_credit">Developed by </p>
	</div>

</body>
</html>