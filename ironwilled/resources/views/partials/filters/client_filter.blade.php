<div class="all_clients_filters">

	<div class="ac_sort_search">
		
		<div class="ac_sort">

			<form class="ac_sort_form" method="GET" action="">
				<h4>Sort By:</h4>
				<select class="ac_sort_options" name="ac_sort_options">
					<option>Title</option>
					<option>Date</option>
				</select>

				<select class="ac_sort_order" name="ac_sort_order">
					<option>Ascending</option>
					<option>Descending</option>
				</select>
				<button type="submit" name="">Sort</button>
			</form>

		</div>

		<div class="ac_search">
			
			<form class="ac_search_form" method="GET" action="">
				<h4>Search:</h4>
				<input class="ac_search_input" type="search" name="ac_search_input">
				<button type="submit" name="">Search</button>
			</form>

		</div>

	</div>

	<div class="ac_filter_page_bulk_action">

		<form class="ac_filter" method="GET" action="">
			<h4>Filter By:</h4>
			<select class="ac_filter_options">
				<option>Author</option>
				<option>Tag</option>
				<option>Series</option>
				<option>Categories</option>
				<option>Publish Status</option>
			</select>
			<input class="ac_filter_input" type="text" name="ac_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="ac_page_filter" method="GET" action="">
			<h4>Page Number:</h4>
			<input class="ac_page_filter_input" type="text" name="ac_page_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="ac_bulk_action" method="GET" action="">
			<h4>Bulk Action:</h4>
			<select name="ac_bulk_action_options">
				<option>Delete All</option>
				<option>Publish All</option>
				<option>Draft All</option>
			</select>
			<button type="button">Go</button>
		</form>

	</div>

</div>