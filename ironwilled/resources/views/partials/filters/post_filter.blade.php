<div class="all_posts_filters">

	<div class="ap_sort_search">
		
		<div class="ap_sort">

			<form class="ap_sort_form" method="GET" action="">
				<h4>Sort By:</h4>
				<select class="ap_sort_options" name="ap_sort_options">
					<option>Title</option>
					<option>Date</option>
				</select>

				<select class="ap_sort_order" name="ap_sort_order">
					<option>Ascending</option>
					<option>Descending</option>
				</select>
				<button type="submit" name="">Sort</button>
			</form>

		</div>

		<div class="ap_search">
			
			<form class="ap_search_form" method="GET" action="">
				<h4>Search:</h4>
				<input class="ap_search_input" type="search" name="ap_search_input">
				<button type="submit" name="">Search</button>
			</form>

		</div>

	</div>

	<div class="ap_filter_page_bulk_action">

		<form class="ap_filter" method="GET" action="">
			<h4>Filter By:</h4>
			<select class="ap_filter_options">
				<option>Author</option>
				<option>Tag</option>
				<option>Series</option>
				<option>Categories</option>
				<option>Publish Status</option>
			</select>
			<input class="ap_filter_input" type="text" name="ap_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="ap_page_filter" method="GET" action="">
			<h4>Page Number:</h4>
			<input class="ap_page_filter_input" type="text" name="ap_page_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="ap_bulk_action" method="GET" action="">
			<h4>Bulk Action:</h4>
			<select name="ap_bulk_action_options">
				<option>Delete All</option>
				<option>Publish All</option>
				<option>Draft All</option>
			</select>
			<button type="button">Go</button>
		</form>

	</div>

</div>