<div class="all_videos_filters">

	<div class="av_sort_search">
		
		<div class="av_sort">

			<form class="av_sort_form" method="GET" action="">
				<h4>Sort By:</h4>
				<select class="av_sort_options" name="av_sort_options">
					<option>Title</option>
					<option>Date</option>
				</select>

				<select class="av_sort_order" name="av_sort_order">
					<option>Ascending</option>
					<option>Descending</option>
				</select>
				<button type="submit" name="">Sort</button>
			</form>

		</div>

		<div class="av_search">
			
			<form class="av_search_form" method="GET" action="">
				<h4>Search:</h4>
				<input class="av_search_input" type="search" name="av_search_input">
				<button type="submit" name="">Search</button>
			</form>

		</div>

	</div>

	<div class="av_filter_page_bulk_action">

		<form class="av_filter" method="GET" action="">
			<h4>Filter By:</h4>
			<select class="av_filter_options">
				<option>Author</option>
				<option>Tag</option>
				<option>Series</option>
				<option>Categories</option>
				<option>Publish Status</option>
			</select>
			<input class="av_filter_input" type="text" name="av_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="av_page_filter" method="GET" action="">
			<h4>Page Number:</h4>
			<input class="av_page_filter_input" type="text" name="av_page_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="av_bulk_action" method="GET" action="">
			<h4>Bulk Action:</h4>
			<select name="av_bulk_action_options">
				<option>Delete All</option>
				<option>Publish All</option>
				<option>Draft All</option>
			</select>
			<button type="button">Go</button>
		</form>

	</div>

</div>