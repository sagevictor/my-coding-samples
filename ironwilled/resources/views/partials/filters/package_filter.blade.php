<div class="all_packages_filters">

	<div class="apa_sort_search">
		
		<div class="apa_sort">

			<form class="apa_sort_form" method="GET" action="">
				<h4>Sort By:</h4>
				<select class="apa_sort_options" name="apa_sort_options">
					<option>Title</option>
					<option>Date</option>
				</select>

				<select class="apa_sort_order" name="apa_sort_order">
					<option>Ascending</option>
					<option>Descending</option>
				</select>
				<button type="submit" name="">Sort</button>
			</form>

		</div>

		<div class="apa_search">
			
			<form class="apa_search_form" method="GET" action="">
				<h4>Search:</h4>
				<input class="apa_search_input" type="search" name="apa_search_input">
				<button type="submit" name="">Search</button>
			</form>

		</div>

	</div>

	<div class="apa_filter_page_bulk_action">

		<form class="apa_filter" method="GET" action="">
			<h4>Filter By:</h4>
			<select class="apa_filter_options">
				<option>Author</option>
				<option>Tag</option>
				<option>Series</option>
				<option>Categories</option>
				<option>Publish Status</option>
			</select>
			<input class="apa_filter_input" type="text" name="apa_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="apa_page_filter" method="GET" action="">
			<h4>Page Number:</h4>
			<input class="apa_page_filter_input" type="text" name="apa_page_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="apa_bulk_action" method="GET" action="">
			<h4>Bulk Action:</h4>
			<select name="apa_bulk_action_options">
				<option>Delete All</option>
				<option>Publish All</option>
				<option>Draft All</option>
			</select>
			<button type="button">Go</button>
		</form>

	</div>

</div>