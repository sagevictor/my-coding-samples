<div class="all_images_filters">

	<div class="img_sort_search">
		
		<div class="img_sort">

			<form class="img_sort_form" method="GET" action="">
				<h4>Sort By:</h4>
				<select class="img_sort_options" name="img_sort_options">
					<option>Title</option>
					<option>Date</option>
				</select>

				<select class="img_sort_order" name="img_sort_order">
					<option>Ascending</option>
					<option>Descending</option>
				</select>
				<button type="submit" name="">Sort</button>
			</form>

		</div>

		<div class="img_search">
			
			<form class="img_search_form" method="GET" action="">
				<h4>Search:</h4>
				<input class="img_search_input" type="search" name="img_search_input">
				<button type="submit" name="">Search</button>
			</form>

		</div>

	</div>

	<div class="img_filter_page_bulk_action">

		<form class="img_filter" method="GET" action="">
			<h4>Filter By:</h4>
			<select class="img_filter_options">
				<option>Author</option>
				<option>Tag</option>
				<option>Series</option>
				<option>Categories</option>
				<option>Publish Status</option>
			</select>
			<input class="img_filter_input" type="text" name="img_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="img_page_filter" method="GET" action="">
			<h4>Page Number:</h4>
			<input class="img_page_filter_input" type="text" name="img_page_filter_input">
			<button type="button">Go</button>
		</form>

		<form class="img_bulk_action" method="GET" action="">
			<h4>Bulk Action:</h4>
			<select name="img_bulk_action_options">
				<option>Delete All</option>
				<option>Publish All</option>
				<option>Draft All</option>
			</select>
			<button type="button">Go</button>
		</form>

	</div>

</div>