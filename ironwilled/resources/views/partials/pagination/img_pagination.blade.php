<div class="all_images_pagination">

	<div class="pagination_center">
		
		<div class="all_images_pagination_begin"> <a>|&lt;&lt;</a> </div>
		<div class="all_images_pagination_prev"> <a>&lt;&lt;</a> </div>

		<div class="pagination_page_numbers"> <a>1</a> <a>2</a> <a>3</a> <a>4</a> <a>5</a> </div>

		<div class="all_images_pagination_next"> <a>&gt;&gt;</a> </div>
		<div class="all_images_pagination_end"> <a>&gt;&gt;|</a> </div>
	
	</div>

</div>