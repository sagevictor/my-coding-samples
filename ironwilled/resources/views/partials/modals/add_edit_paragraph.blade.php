<div class="add_edit_paragraph_modal">

	<div class="aep_modal_title">

		<h2><strong>Add/Edit Paragraph</strong></h2>
		<span class="aep_modal_close">&times;</span>

	</div>

	<div class="aep_modal_main_area">
		<textarea></textarea>
	</div>

	<div class="aep_modal_buttons">

		<button type="button">Confirm</button>
		<button type="button">Cancel</button>
		
	</div>
	
</div>