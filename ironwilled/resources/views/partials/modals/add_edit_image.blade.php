<div class="add_edit_image_modal">

	<div class="aei_modal_title">

		<h2><strong>Add/Edit Image</strong></h2>
		<span class="aei_modal_close">&times;</span>

	</div>

	<div class="aei_modal_main_area">
		
		<div class="aei_image_upload">
			
		</div>

		<div class="aei_image_index">

			<div class="aei_image_search"><input type="text" name=""></div>

			<div class="aei_image_list">

				<div class="aei_list_result">
					
					<div class="aei_list_title">

						<div class="aei_list_title_left">
							<h3><strong>Image Title</strong></h3>
						</div>

						<div class="aei_list_title_right">
							<p>View Image</p>
						</div>

					</div>

					<div class="aei_list_details">
						
						<div class="aei_list_details_img">
							<a href=""><img src=""/></a>
						</div>

						<div class="aei_list_details_info">

							<p><a href="">Author</a></p>
							<p><a href="">01-01-2018</a></p>
							
						</div>

					</div>

				</div>
				
			</div>
			
		</div>

	</div>

	<div class="aei_modal_buttons">

		<button type="button">Confirm</button>
		<button type="button">Cancel</button>
		
	</div>
	
</div>