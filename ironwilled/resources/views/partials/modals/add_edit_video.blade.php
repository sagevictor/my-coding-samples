<div class="add_edit_video_modal">

	<div class="aev_modal_title">

		<h2><strong>Add/Edit Video</strong></h2>
		<span class="aev_modal_close">&times;</span>

	</div>

	<div class="aev_modal_main_area">
		
		<div class="aev_video_upload">
			
		</div>

		<div class="aev_video_index">

			<div class="aev_video_search"><input type="text" name=""></div>

			<div class="aev_video_list">

				<div class="aev_list_result">
					
					<div class="aev_list_title">

						<div class="aev_list_title_left">
							<h3><strong>Video Title</strong></h3>
						</div>

						<div class="aev_list_title_right">
							<p>View Video</p>
						</div>

					</div>

					<div class="aev_list_details">
						
						<div class="aev_list_details_img">
							<a href="">
								<img src=""/>
							</a>
						</div>

						<div class="aev_list_details_info">

							<p><a href="">Author</a></p>
							<p><a href="">01-01-2018</a></p>
							
						</div>

					</div>

				</div>
				
			</div>
			
		</div>

	</div>

	<div class="aev_modal_buttons">

		<button type="button">Confirm</button>
		<button type="button">Cancel</button>
		
	</div>
	
</div>