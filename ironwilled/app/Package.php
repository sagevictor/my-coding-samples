<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //
    protected $fillable=['title', 'slug', 'description', 'status'];
    
}
