<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $adminUser = new User();
        $adminUser->name = 'pvictor';
        $adminUser->email = 'phevic1@gmail.com';
        $adminUser->password = bcrypt('abc123');
        $adminUser->save();
    }
}
